<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181128012513 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE elevator
            (
                name VARCHAR(80) PRIMARY KEY NOT NULL,
                weight INT,
                `order` INT
            )
        ');

        $this->addSql("
            INSERT INTO elevator (name, weight, `order`) VALUES 
                ('Sheridan Thatcher', 400, 2),
                ('Robert Pruitt', 100, 1), 
                ('Gina Rosas', 200, 3),
                ('Tayyab Ryan', 100, 4), 
                ('Amelia-Lily Mustafa', 400, 5), 
                ('Kingston Correa', 500, 6), 
                ('Lorenzo Wilkinson', 300, 7)
        ");

        $this->addSql('
            CREATE TABLE invoice
            (
                invoice_id TINYINT UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
                client_id TINYINT UNSIGNED NOT NULL,
                price MEDIUMINT UNSIGNED NOT NULL,
                created_at DATE
            )
        ');

        $this->addSql("
            INSERT INTO invoice (client_id, price, created_at) VALUES 
                (42, 875, '2018-02-01'),
                (60, 3069, '2018-01-01'),
                (60, 1920, '2018-02-01'),
                (60, 3484, '2018-03-01'),
                (60, 2440, '2018-05-01'),
                (18, 4442, '2018-01-01'),
                (18, 3402, '2018-02-01'),
                (18, 3059, '2018-03-01'),
                (42, 2630, '2018-01-01')
        ");
    }

    public function down(Schema $schema) : void
    {
    }
}
