<?php

use \Spatie\Snapshots\MatchesSnapshots;
use \Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ElevatorTest extends KernelTestCase
{
    use MatchesSnapshots;

    /** @var \Doctrine\DBAL\Connection */
    private $connection;

    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->connection = $kernel->getContainer()->get('doctrine')->getConnection();
    }

    /**
     * A partir des personnes de la table elevator, définir dans quel ascenceur monterons les gens,
     * l'ascenceur n'accepte pas plus de 700kg
     */
    public function testElevatorPack(): void
    {
        // todo ici on va mettre le blabla de l'ascenceur
        // todo ou mettre le blabla dans un readme
        $sql = <<<SQL
          SELECT * FROM elevator
SQL;

        $result = $this->connection->executeQuery($sql)->fetchAll();

        $this->assertMatchesJsonSnapshot(json_encode($result));
    }
}