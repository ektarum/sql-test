<?php

use \Spatie\Snapshots\MatchesSnapshots;
use \Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class InvoiceTest extends KernelTestCase
{
    use MatchesSnapshots;

    /** @var \Doctrine\DBAL\Connection */
    private $connection;

    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->connection = $kernel->getContainer()->get('doctrine')->getConnection();
    }

    /**
     * On cherche a récupéré pour chaque invoice du client, le price cumulé depuis le départ pour ce client
     * et le price total pour ce client (et cela pour chaque ligne.
     */
    public function testPrices(): void
    {
        $sql = <<<SQL
          SELECT * FROM invoice
SQL;

        $result = $this->connection->executeQuery($sql)->fetchAll();

        $this->assertMatchesJsonSnapshot(json_encode($result));
    }
}