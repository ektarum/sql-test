FIG=docker-compose
HAS_DOCKER:=$(shell command -v $(FIG) 2> /dev/null)
ifdef HAS_DOCKER
	EXEC=$(FIG) exec app
	RUN=$(FIG) run --rm app
else
	EXEC=
	RUN=
endif
CONSOLE=php bin/console

.DEFAULT_GOAL := help

##
## Project setup & day to day shortcuts
##---------------------------------------------------------------------------

.PHONY: help ## Generate list of targets with descriptions
help:
	@grep '##' Makefile \
	| grep -v 'grep\|sed' \
	| sed 's/^\.PHONY: \(.*\) ##[\s|\S]*\(.*\)/\1:\t\2/' \
	| sed 's/\(^##\)//' \
	| sed 's/\(##\)/\t/' \
	| expand -t14

.PHONY: start
start: build docker-compose.override.yml up deps perm db-wait db-up

.PHONY: stop ## Stop docker containers
stop:
	$(FIG) down

.PHONY: restart
restart: stop up perm

.PHONY: clear
clear: perm
	$(EXEC) rm -rf public/assets/*
	$(EXEC) rm -rf var/cache/*
	$(EXEC) rm -rf var/sessions/*
	$(EXEC) rm -rf var/logs/*
	$(EXEC) rm -rf .env docker-compose.override.yml
	$(EXEC) rm -rf vendor

.PHONY: destroy ## Destroy docker containers and volumes
destroy:
	$(FIG) down -v

.PHONY: uninstall
uninstall: clear destroy

.PHONY: cc ## Clear the cache in dev env
cc:
	$(RUN) $(CONSOLE) cache:clear --no-warmup
	$(RUN) $(CONSOLE) cache:warmup

.PHONY: exec ## Run bash in the app container
exec:
	$(EXEC) /bin/bash

.PHONY: logs ## Display nginx/php-fpm logs
logs-web:
	$(FIG) logs -f nginx app

##
## Databases
##---------------------------------------------------------------------------

.PHONY: db-wait
db-wait:
	$(EXEC) php -r "while(!@fsockopen('db',3306)){}"

.PHONY: db-up
db-up:
	$(EXEC) $(CONSOLE) doctrine:database:create --if-not-exists
	$(EXEC) $(CONSOLE) doctrine:migrations:migrate --allow-no-migration -n -q

##
## Tests
##---------------------------------------------------------------------------

.PHONY: tests ## Run ALL test
tests:
	$(EXEC) vendor/bin/phpunit

##
## Dependencies
##---------------------------------------------------------------------------

.PHONY: deps ## Install the project PHP and JS dependencies
deps: vendor


# Internal rules

build:
	$(FIG) pull
	$(FIG) build

up: ## up command
	$(FIG) up -d

.PHONY: perm
perm:
	$(EXEC) chmod -R 777 var || exit 0

.PHONY: vendor
vendor:
	$(RUN) composer install --prefer-dist --no-progress --no-suggest

# Rules from files
docker-compose.override.yml: docker-compose.override.yml.dist
	$(RUN) cp docker-compose.override.yml.dist docker-compose.override.yml

.env:
	$(RUN) cp .env.dist .env
